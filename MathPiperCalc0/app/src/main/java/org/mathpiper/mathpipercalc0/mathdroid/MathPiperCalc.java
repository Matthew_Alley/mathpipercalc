package org.mathpiper.mathpipercalc0.mathdroid;

import android.app.*;
import android.content.*;
import android.os.Bundle;
import android.preference.*;
import android.text.*;
import android.util.Log;
import android.view.*;
import android.view.inputmethod.*;
import android.widget.*;

import java.io.File;
import java.io.InputStream;
import java.util.*;
import android.support.v7.app.ActionBarActivity;

import org.mathpiper.interpreters.EvaluationResponse;
import org.mathpiper.interpreters.Interpreter;
import org.mathpiper.interpreters.ResponseListener;
import org.mathpiper.interpreters.Utilities;
import org.mathpiper.lisp.cons.Cons;
import org.mathpiper.mathpipercalc0.R;
import org.mathpiper.mathpipercalc0.mathdroid.history.HistoryAdapter;
import org.mathpiper.mathpipercalc0.mathdroid.history.HistoryCalculationItem;
import org.mathpiper.mathpipercalc0.mathdroid.history.HistoryItem;
import org.mathpiper.mathpipercalc0.mathdroid.history.HistoryProgramItems;
import org.mathpiper.test.Fold;
import org.mathpiper.ui.gui.android.ExerciseItemListActivity;
import org.mathpiper.ui.gui.android.ProgramItemListActivity;
import org.mathpiper.ui.gui.android.ProgramManager;

import ar.com.daidalos.afiledialog.FileChooserActivity;


public class MathPiperCalc extends ActionBarActivity implements AdapterView.OnItemClickListener, TextView.OnEditorActionListener, View.OnClickListener, ResponseListener {
    private static final String TAG = "MathPiperCalc";

    // Constants for the transcript context menu items.
    private static final int CONTEXT_MENU_RETYPE_SELECTED = 0;
    private static final int CONTEXT_MENU_COPY_SELECTED = 1;
    private static final int CONTEXT_MENU_COPY_ALL  = 2;
    private static final int CONTEXT_MENU_FORGET_SELECTED = 3;
    private static final int CONTEXT_MENU_FORGET_ALL  = 4;

    // Constants identifying dialogs.
    private static final int DIALOG_PLOT = 0;

    private final HashMap<Integer, String> buttonMap = new HashMap<Integer, String>();

    private HistoryAdapter history;

    private boolean continuationMode;
    private boolean hapticFeedback;

    private Map<String, Fold> foldsMap;

    private Interpreter synchronousInterpreter;

    private Interpreter asynchronousInterpreter;

    private String programName = "";
    private String programCode = "";
    private String programFilename = "";
    //private List<String> parameterList = null;
    private List parametersKeys = null;
    private List parametersValues = null;
    private int parametersCount = 0;


    private String mode = "calculation";
    private HistoryProgramItems currentHistoryProgramItems = null;

    // Called when the activity is first created or recreated.
    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
 //todo:tk
        Compatibility compatibility = Compatibility.get();
        compatibility.configureActionBar(this);
        if (!compatibility.isTablet(this)) {
            // If we hide the title bar on modern releases, that hides the ActionBar,
            // and then the user has no way to get to the settings or help.
            if (!compatibility.hasActionBar()) {
                // Hide the title bar if we're on a small screen and don't want to waste space.
                // We can't do this in the manifest because we want it conditional on screen size
                // and OS version.
                requestWindowFeature(Window.FEATURE_NO_TITLE);
            }
            // On a small screen, we want the system keyboard to overlap ours, not cause resizing.
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        }

        asynchronousInterpreter = org.mathpiper.interpreters.Interpreters.getAsynchronousInterpreter();
        asynchronousInterpreter.addResponseListener(this);

        synchronousInterpreter = org.mathpiper.interpreters.Interpreters.getSynchronousInterpreter();

        setContentView(R.layout.main);

        final EditText queryView = (EditText) findViewById(R.id.query);
        queryView.setOnEditorActionListener(this);
        queryView.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                onQueryTextChanged(queryView);
            }
            public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
            public void onTextChanged(CharSequence s, int start, int before, int count) { }
        });
        queryView.requestFocus();

        // Prevent the soft keyboard from appearing unless the user presses our keyboard button.
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        // Android doesn't count the _soft_ keyboard appearing/disappearing as a configChange.
        // This is the stackoverflow-approved hack for detecting soft keyboard visibility changes.
        // http://stackoverflow.com/questions/2150078/how-to-check-visibility-of-software-keyboard-in-android
        final View activityRootView = findViewById(android.R.id.content);
        activityRootView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
          @Override public void onGlobalLayout() {
            int heightDiff = activityRootView.getRootView().getHeight() - activityRootView.getHeight();
            findViewById(R.id.on_screen_keyboard).setVisibility((heightDiff < activityRootView.getHeight()/3) ? View.VISIBLE : View.GONE);
          }
        });

        initButtonMap();

        initButtonClickListener(R.id.del);
        initButtonClickListener(R.id.exe);
        initButtonClickListener(R.id.kbd);
        initButtonClickListener(R.id.left);
        initButtonClickListener(R.id.flip);
        initButtonClickListener(R.id.right);

        for (int id : buttonMap.keySet()) {
            initButtonClickListener(id);
        }

        this.history = new HistoryAdapter(this);

        ListView transcriptView = transcriptView();
        registerForContextMenu(transcriptView);
        transcriptView.setAdapter(history);
        transcriptView.setOnItemClickListener(this);

        try {
            loadState();
        } catch (Exception ex) {
            // Without code like this, it's impossible to recover from bad state: Mathdroid will just crash every time you start it.
            ex.printStackTrace();
        }

        onConfigurationChanged(getResources().getConfiguration());
    }

    private void initButtonClickListener(int id) {
        // Not all buttons will be present in all layouts.
        final View button = findViewById(id);
        if (button != null) {
            button.setOnClickListener(this);
        }
    }

    private void initButtonMap() {
        buttonMap.put(R.id.acos,   "acos()");
        buttonMap.put(R.id.res,    "#");
        buttonMap.put(R.id.asin,   "asin()");
        buttonMap.put(R.id.atan,   "atan()");
        buttonMap.put(R.id.ceil,   "ceil()");
        buttonMap.put(R.id.close,  ")");
        buttonMap.put(R.id.comma,  ",");
        buttonMap.put(R.id.cos,    "cos()");
        buttonMap.put(R.id.digit0, "0");
        buttonMap.put(R.id.digit1, "1");
        buttonMap.put(R.id.digit2, "2");
        buttonMap.put(R.id.digit3, "3");
        buttonMap.put(R.id.digit4, "4");
        buttonMap.put(R.id.digit5, "5");
        buttonMap.put(R.id.digit6, "6");
        buttonMap.put(R.id.digit7, "7");
        buttonMap.put(R.id.digit8, "8");
        buttonMap.put(R.id.digit9, "9");
        buttonMap.put(R.id.divide, "/");
        buttonMap.put(R.id.dot,    ".");
        buttonMap.put(R.id.e,      "e");
        buttonMap.put(R.id.floor,  "floor()");
        buttonMap.put(R.id.log10,  "Log10()");
        buttonMap.put(R.id.logE,   "LogE()");
        buttonMap.put(R.id.minus,  "-");
        buttonMap.put(R.id.ncr,    "nCr()");
        buttonMap.put(R.id.npr,    "nPr()");
        buttonMap.put(R.id.open,   "(");
        buttonMap.put(R.id.pi,     "\u03c0");
        buttonMap.put(R.id.pling,  "!");
        buttonMap.put(R.id.plus,   "+");
        buttonMap.put(R.id.pow,    "^");
        buttonMap.put(R.id.rand,   "rand()");
        buttonMap.put(R.id.sin,    "sin()");
        buttonMap.put(R.id.sqrt,   "\u221a");
        buttonMap.put(R.id.tan,    "tan()");
        buttonMap.put(R.id.times,  "*");
        buttonMap.put(R.id.x,      "x");
    }

    @Override public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.options, menu);
        return true;
    }

    @Override public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
        case R.id.menu_clear:
            clear((EditText) findViewById(R.id.query));
            return true;
        case R.id.menu_help:
            showHelp();
            return true;
        case R.id.menu_settings:
            showSettings();
            return true;
        /*case R.id.menu_editor:
            showEditor();
            return true;*/
        case R.id.menu_programs:
            showPrograms();
            return true;
        case R.id.menu_exercises:
            showExercises();
            return true;
        case R.id.menu_folders:
            showFolders();
            return true;
        default:
            return super.onOptionsItemSelected(item);
        }
    }

    @Override protected Dialog onCreateDialog(int id) {
        switch (id) {
        case DIALOG_PLOT:
            return createPlotDialog();
        default:
            return null;
        }
    }

    @Override protected void onPrepareDialog(int id, Dialog dialog) {
        switch (id) {
        case DIALOG_PLOT:
            // final PlotView plotView = (PlotView) dialog.findViewById(R.id.plot);
           //  plotView.preparePlot(calculator, plotData);
            //plotData = null;
            return;
        default:
            return;
        }
    }

    private Dialog createPlotDialog() {
        final LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        final View layout = inflater.inflate(R.layout.plot, (ViewGroup) findViewById(R.id.plot));

        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(layout);
        builder.setCancelable(true);

        final Dialog dialog = builder.create();
        return dialog;
    }

    private HistoryItem selectedHistoryItem(ContextMenu.ContextMenuInfo menuInfo) {
        if (!(menuInfo instanceof AdapterView.AdapterContextMenuInfo)) {
            return null;
        }
        final int position = ((AdapterView.AdapterContextMenuInfo) menuInfo).position;
        return history.getItem(position);
    }

    @Override public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        if (history.getCount() == 0) {
            // If there's no transcript, there's nothing to copy, so no reason to show a menu.
            return;
        }

        super.onCreateContextMenu(menu, v, menuInfo);
        menu.setHeaderTitle("History");
        final HistoryItem historyItem = selectedHistoryItem(menuInfo);
        if (historyItem != null) {
            menu.add(0, CONTEXT_MENU_RETYPE_SELECTED, 0, "Retype '" + historyItem.getFirst() + "'");
            if(historyItem instanceof HistoryCalculationItem) {
                menu.add(0, CONTEXT_MENU_COPY_SELECTED, 0, "Copy '" + historyItem.getFirst() + " = " + historyItem.getSecond() + "'");
            }
            else
            {
                menu.add(0, CONTEXT_MENU_COPY_SELECTED, 0, "Copy Program Interaction");
            }
        }
        menu.add(0, CONTEXT_MENU_COPY_ALL,  0, "Copy all");
        if (historyItem != null) {
            if(historyItem instanceof HistoryCalculationItem) {
                menu.add(0, CONTEXT_MENU_FORGET_SELECTED, 0, "Forget '" + historyItem.getFirst() + " = " + historyItem.getSecond() + "'");
            }
            else {
                menu.add(0, CONTEXT_MENU_FORGET_SELECTED, 0, "Forget Program Interaction");
            }
        }
        menu.add(0, CONTEXT_MENU_FORGET_ALL,  0, "Forget all");
    }

    @Override public boolean onContextItemSelected(MenuItem item) {
        final int id = item.getItemId();
        final ContextMenu.ContextMenuInfo menuInfo = item.getMenuInfo();
        final HistoryItem historyItem = selectedHistoryItem(menuInfo);
        switch (id) {
        case CONTEXT_MENU_RETYPE_SELECTED:
            final EditText queryView = (EditText) findViewById(R.id.query);
            queryView.setText(historyItem.getFirst());
            queryView.setSelection(queryView.length());
            return true;
        case CONTEXT_MENU_COPY_SELECTED:
            if(historyItem instanceof HistoryCalculationItem) {
                return copyToClipboard(historyItem.getFirst() + " = " + historyItem.getSecond());
            }
            else
            {
                return copyToClipboard(historyItem.prettyPrint());
            }
        case CONTEXT_MENU_COPY_ALL:
            return copyToClipboard(history.toString());
        case CONTEXT_MENU_FORGET_SELECTED:
            history.remove(((AdapterView.AdapterContextMenuInfo) menuInfo).position);
            return true;
        case CONTEXT_MENU_FORGET_ALL:
            history.clear();
            return true;
        default:
            return super.onContextItemSelected(item);
        }
    }

    @SuppressWarnings("deprecation") // Honeycomb replaces the text-only ClipboardManager.
    private boolean copyToClipboard(String text) {
        final android.text.ClipboardManager clipboard = (android.text.ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
        clipboard.setText(text);
        return true;
    }

    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        final EditText queryView = (EditText) findViewById(R.id.query);
        final HistoryItem historyItem = history.getItem(position);
        queryView.setText(historyItem.getFirst());
        queryView.setSelection(queryView.length());
    }

    @Override protected void onPause() {
        super.onPause();
        saveState();
    }

    @Override public void onResume() {
        super.onResume();
        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);

        String angleMode = settings.getString("angleMode", "Radians");
        // calculator.setDegreesMode(angleMode.equals("Degrees"));

        String outputBase = settings.getString("outputBase", "10");
        // calculator.setOutputBase(Integer.parseInt(outputBase));

        this.continuationMode = settings.getBoolean("continuationMode", false);
        this.hapticFeedback = settings.getBoolean("hapticFeedback", false);

        final EditText queryView = (EditText) findViewById(R.id.query);
        queryView.selectAll();
    }

    private void performHapticFeedback(View view) {
        if (!hapticFeedback) {
            return;
        }
        int HapticFeedbackConstants_VIRTUAL_KEY = 1; // HapticFeedbackConstants.VIRTUAL_KEY not available until API 5.
        int SDK_INT = Integer.parseInt(android.os.Build.VERSION.SDK); // SDK_INT not available until API 4.
        int type = SDK_INT >= 5 ? HapticFeedbackConstants.LONG_PRESS : HapticFeedbackConstants_VIRTUAL_KEY;
        view.performHapticFeedback(type, HapticFeedbackConstants.FLAG_IGNORE_GLOBAL_SETTING);
    }

    @Override public void onClick(View view) {
        performHapticFeedback(view);
        final EditText queryView = (EditText) findViewById(R.id.query);
        final int id = view.getId();
        switch (id) {
        case R.id.transcript:
            break;
        case R.id.kbd:
            showSoftKeyboard(queryView);
            break;
        case R.id.left:
            moveCaret(queryView, -1);
            break;
        case R.id.right:
            moveCaret(queryView, 1);
            break;
        case R.id.del:
            del(queryView);
            break;
        case R.id.exe:
            exe(queryView);
            break;
        case R.id.flip:
            ((ViewFlipper) findViewById(R.id.flipper2)).showNext();
            break;
        default:
            buttonPressed(queryView, id);
        }
    }

    // Invoked if the user hits the physical "return" key while the EditText has focus.
    public boolean onEditorAction(TextView queryView, int actionId, KeyEvent event) {
        if (event != null && event.getAction() == KeyEvent.ACTION_UP) {
            // We already handled the ACTION_DOWN event, and don't want to repeat the work.
            return true;
        }
        exe((EditText) queryView);
        return true;
    }

    private void buttonPressed(EditText queryView, int id) {
        // Insert the new text (by replacing the entire content, which is our only option).
        // If there's a selection, we overwrite it.
        final String newText = buttonMap.get(id);
        final String existingText = queryView.getText().toString();
        final int startOffset = queryView.getSelectionStart();
        final int endOffset = queryView.getSelectionEnd();
        queryView.setText(existingText.substring(0, startOffset) + newText + existingText.substring(endOffset));
        // Put the caret back in the right place.
        int newCaretOffset;
        if (newText.length() > 1 && newText.endsWith(")")) {
            // For functions, we automatically insert both parentheses, so we need to move the caret back between them.
            newCaretOffset = startOffset + newText.length() - 1;
        } else {
            newCaretOffset = startOffset + newText.length();
        }
        queryView.setSelection(newCaretOffset, newCaretOffset);
    }

    private void clear(EditText queryView) {
        queryView.setText("");
        history.clear();


        programName = "";
        programCode = "";
        programFilename = "";
        this.parametersCount = 0;
        this.parametersKeys = null;
        this.parametersKeys = null;

        mode = "calculation";
        currentHistoryProgramItems = null;

        android.support.v7.app.ActionBar actionBar = this.getSupportActionBar();
        if(actionBar != null)
        {
            actionBar.setSubtitle("Mode:Calculation");
        }
    }

    private void showSoftKeyboard(EditText queryView) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(queryView, 0);
    }

    private void del(EditText queryView) {
        int startOffset = queryView.getSelectionStart();
        int endOffset = queryView.getSelectionEnd();
        final String existingText = queryView.getText().toString();
        if (existingText.length() == 0) {
            return;
        }
        if (startOffset != endOffset) {
            // Remove the selection.
            queryView.setText(existingText.substring(0, startOffset) + existingText.substring(endOffset));
        } else {
            // Remove the character before the caret.
            if (startOffset == 0) {
                // There is no character before the caret.
                return;
            }
            --startOffset;
            queryView.setText(existingText.substring(0, startOffset) + existingText.substring(endOffset));
        }
        queryView.setSelection(startOffset, startOffset);
    }

    private void moveCaret(EditText queryView, int delta) {
        int offset = (delta > 0) ? queryView.getSelectionEnd() : queryView.getSelectionStart();
        offset += delta;
        offset = Math.max(Math.min(offset, queryView.length()), 0);
        queryView.setSelection(offset);
    }

    private void onQueryTextChanged(final EditText queryView) {
        if (!continuationMode) {
            return;
        }
        // Implement continuation mode, where we insert "Ans" before an operator at the start of an expression.
        // Note that this is ambiguous in the case of '-' which may be part of a numeric literal starting a new expression, or subtraction, indicating a continuation.
        // This is the price people who want this feature have to pay.
        final String newInput = queryView.getText().toString();
        if (isContinuationOperator(newInput)) {
            // We could actually include the text of the previous expression, but you can just click the history item to get that behavior.
            // String lastInput = history.getItem(history.getCount() - 1).question;
            String lastInput = "#";
            final String replacement = lastInput + newInput;
            queryView.post(new Runnable() {
                public void run() {
                    // We have to do this later on the UI thread, or the selection change doesn't happen.
                    queryView.setText(replacement);
                    queryView.setSelection(replacement.length());
                }
            });
        }
    }

    private static boolean isContinuationOperator(String s) {
        return s.length() == 1 && "-=+*^/%<>&|".indexOf(s.charAt(0)) != -1;
    }

    private void exe(EditText queryView) {

        final String queryText = queryView.getText().toString().trim();

        if (queryText.length() == 0) {
            // Nothing to do. Finger flub.
            return;
        }

        if(mode.equals("program")) // currentParameter != null.
        {
            String code = org.mathpiper.interpreters.Utilities.toJavaString((String)parametersKeys.get(parametersCount++)) + " := " + queryText + ";";
            EvaluationResponse evaluationResponse = evaluate(code);

            String result;

            if (evaluationResponse.isExceptionThrown()) {
                result = evaluationResponse.getException().getMessage();

                // todo:tk:handle exceptions.
            }
            else {
                result = evaluationResponse.getResult();
            }


            if (continuationMode) {
                // Clear the input; we may put it back depending on what the user types next...
                queryView.setText("");
            } else {
                // Select the input to make it easy to replace while allowing the possibility of further editing.
                queryView.selectAll();
            }
            // Adding to the history automatically updates the display.

            this.currentHistoryProgramItems.add(result + ":inputValue");
            history.notifyDataSetChanged();

            if(this.parametersKeys != null && this.parametersCount < this.parametersKeys.size()) {
                this.currentHistoryProgramItems.add(org.mathpiper.interpreters.Utilities.toJavaString((String)parametersValues.get(parametersCount)) + ":inputQuestion");
                history.notifyDataSetChanged();
            }
            else
            {
                EvaluationResponse evaluationResponse2 = evaluate(this.programCode);



                if (evaluationResponse.isExceptionThrown()) {
                    result = evaluationResponse2.getException().getMessage();

                }
                else {
                    result = evaluationResponse2.getResult();
                }


                if (continuationMode) {
                    // Clear the input; we may put it back depending on what the user types next...
                    queryView.setText("");
                } else {
                    // Select the input to make it easy to replace while allowing the possibility of further editing.
                    queryView.selectAll();
                }

                this.currentHistoryProgramItems.add(result + ":answer");
                history.notifyDataSetChanged();

                queryView.setText(result);
                queryView.selectAll();

                this.mode = "calculation";
                this.parametersCount = 0;
                this.parametersKeys = null;
                this.parametersValues = null;
                android.support.v7.app.ActionBar actionBar = this.getSupportActionBar();

                if(actionBar != null)
                {
                    actionBar.setSubtitle("Mode:Calculation");
                }
            }


        }
        else
        {
            // Calculation mode.

            if (continuationMode) {
                // Clear the input; we may put it back depending on what the user types next...
                queryView.setText("");
            } else {
                // Select the input to make it easy to replace while allowing the possibility of further editing.
                queryView.selectAll();
            }
            // Adding to the history automatically updates the display.
            history.add(new HistoryCalculationItem(queryText, computeAnswer(queryText)));
        }
    }

    private String computeAnswer(String query) {

        EvaluationResponse evaluationResponse = synchronousInterpreter.evaluate(query + ";");

        String result = "ERROR";

        if (evaluationResponse.isExceptionThrown()) {
            result = evaluationResponse.getException().getMessage();

        }
        else {
            result = evaluationResponse.getResult();
        }

        return result;
    }

    private void loadState() {
        final SharedPreferences state = getPreferences(MODE_PRIVATE);
        final int version = state.getInt("version", 0);
        if (version != 3) {
            // We've never been run before, or the last run was an incompatible version.
            return;
        }

        final EditText queryView = (EditText) findViewById(R.id.query);
        final String oldQuery = state.getString("query", "");
        queryView.setText(oldQuery);
        queryView.selectAll();

        final String serializedHistory = state.getString("transcript", "");
        history.fromString(serializedHistory);
    }

    private void saveState() {
        final String serializedHistory = history.toString();

        final EditText queryView = (EditText) findViewById(R.id.query);

        final SharedPreferences.Editor state = getPreferences(MODE_PRIVATE).edit();
        state.putInt("version", 3);
        state.putString("query", queryView.getText().toString());
        state.putString("transcript", serializedHistory);
        state.commit();
    }

    private ListView transcriptView() {
        return (ListView) findViewById(R.id.transcript);
    }

    private void showHelp() {
        startActivity(new Intent(this, MathPiperCalcHelp.class));
    }

    private void showSettings() {
        startActivity(new Intent(this, MathPiperCalcSettings.class));
    }

    /*
    private void showEditor() {
        startActivity(new Intent(this, MainActivity.class));
    }
    */

    private void showPrograms() {
        Intent intent = new Intent(this, ProgramItemListActivity.class);
        this.startActivityForResult(intent,1);
    }

    private void showExercises() {
        startActivity(new Intent(this, ExerciseItemListActivity.class));
    }


    private void showFolders() {
        Intent intent = new Intent(MathPiperCalc.this, FileChooserActivity.class);

        // Activate the folder mode.
        intent.putExtra(FileChooserActivity.INPUT_FOLDER_MODE, true);

        // Call the activity
        MathPiperCalc.this.startActivityForResult(intent, 2);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request we're responding to
        if (requestCode == 1) {
            // Make sure the request was successful
            if (resultCode == Activity.RESULT_OK) {

                String id = data.getStringExtra("programItemID");


                ProgramManager.ProgramInformation programInfo = ProgramManager.ITEM_MAP.get(id);

                programName = programInfo.getName();

                this.programFilename = programInfo.getFilename();

                android.support.v7.app.ActionBar actionBar = this.getSupportActionBar();

                if(actionBar != null)
                {
                    actionBar.setSubtitle("Mode:" + programName);
                }

                initialize();


                if(foldsMap.containsKey("parameters"))
                {
                    Fold parametersFold = foldsMap.get("parameters");

                    String parameters = parametersFold.getContents();

                    EvaluationResponse evaluationResponse = synchronousInterpreter.evaluate(parameters);


                    if (evaluationResponse.isExceptionThrown()) {
                        // todo:handle exception.

                    }

                    Cons cons = evaluationResponse.getResultList();


                    try {
                        parametersKeys = org.mathpiper.interpreters.Utilities.associationListKeys(synchronousInterpreter.getEnvironment(), -1, cons);
                        parametersValues = org.mathpiper.interpreters.Utilities.associationListValues(synchronousInterpreter.getEnvironment(), -1, cons);



                        if(parametersKeys.size() > 0) {
                            parametersCount = 0;

                            this.currentHistoryProgramItems = new HistoryProgramItems(programName + ":programName");

                            this.currentHistoryProgramItems.add(org.mathpiper.interpreters.Utilities.toJavaString((String) parametersValues.get(parametersCount)) + ":inputQuestion");

                            history.add(this.currentHistoryProgramItems);

                            mode = "program";

                        }
                        else
                        {
                            // todo:tk:handle error.
                        }
                    }
                    catch(Throwable t)
                    {
                        t.printStackTrace(); // todo:tk:handle this exception.
                    }

                }



                if(foldsMap.containsKey("buttons"))
                {
                    Fold buttonsFold = foldsMap.get("buttons");

                    String buttons = buttonsFold.getContents();

                    EvaluationResponse evaluationResponse = synchronousInterpreter.evaluate(buttons);


                    if (evaluationResponse.isExceptionThrown()) {
                        // todo:handle exception.

                    }

                    Cons cons = evaluationResponse.getResultList();


                    try {
                        final List buttonsKeys = org.mathpiper.interpreters.Utilities.associationListKeys(synchronousInterpreter.getEnvironment(), -1, cons);
                        final List buttonsValues = org.mathpiper.interpreters.Utilities.associationListValues(synchronousInterpreter.getEnvironment(), -1, cons);


                        if(buttonsKeys.size() > 0) {
                            int buttonsCount = 0;

                            ButtonsViewFlipper buttonsViewFlipper = (ButtonsViewFlipper) findViewById(R.id.flipper2);

                            ButtonsView buttonsView = new ButtonsView(buttonsViewFlipper.getContext());

                            final EditText queryView = (EditText) findViewById(R.id.query);
                            final String queryText = queryView.getText().toString().trim();

                            if (queryText.length() == 0) {
                                // Nothing to do. Finger flub.
                                return;
                            }

                            while(buttonsCount < buttonsKeys.size())
                            {
                                Button button = new Button(buttonsView.getContext());
                                button.setText(Utilities.toJavaString((String) buttonsKeys.get(buttonsCount)));
                                final int buttonsCountFinal = buttonsCount;
                                button.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        queryView.setText(Utilities.toJavaString((String) buttonsValues.get(buttonsCountFinal)) + "(" + queryText + ")");
                                    }
                                });
                                button.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                                buttonsView.addView(button);

                                buttonsCount++;
                            }



                            buttonsViewFlipper.addButtons(buttonsView);

                            buttonsViewFlipper.showNext();


                        }
                        else
                        {
                            // todo:tk:handle error.
                        }
                    }
                    catch(Throwable t)
                    {
                        t.printStackTrace(); // todo:tk:handle this exception.
                    }

                }




            }
        }
        else if (requestCode == 2) {
            // Make sure the request was successful
            if (resultCode == Activity.RESULT_OK) {
                boolean fileCreated = false;
                String filePath = "";

                Bundle bundle = data.getExtras();
                if(bundle != null)
                {
                    if(bundle.containsKey(FileChooserActivity.OUTPUT_NEW_FILE_NAME)) {
                        fileCreated = true;
                        File folder = (File) bundle.get(FileChooserActivity.OUTPUT_FILE_OBJECT);
                        String name = bundle.getString(FileChooserActivity.OUTPUT_NEW_FILE_NAME);
                        filePath = folder.getAbsolutePath() + "/" + name;
                    } else {
                        fileCreated = false;
                        File file = (File) bundle.get(FileChooserActivity.OUTPUT_FILE_OBJECT);
                        filePath = file.getAbsolutePath();
                    }
                }

                String message = fileCreated? "File created" : "File opened";
                message += ": " + filePath;
                Toast toast = Toast.makeText(MathPiperCalc.this, message, Toast.LENGTH_LONG);
                toast.show();
            }
        }

    }






    @Override
    public void response(EvaluationResponse evaluationResponse) {

        if (evaluationResponse.isExceptionThrown()) {
            String result = evaluationResponse.getException().getMessage();


        } else {
            final String result = evaluationResponse.getResult();

            final EditText queryView = (EditText) findViewById(R.id.query);

        }



    }

    @Override
    public boolean remove() {
        return false;
    }



    public synchronized EvaluationResponse evaluate(String input)
    {

        input = input.replace("\\", "\\\\");
        input = input.replace("\"", "\\\"");

        String inputText = "LoadScript(\"" + input + "\");";

        EvaluationResponse evaluationResponse = synchronousInterpreter.evaluate(inputText);

        return evaluationResponse;
    }


    public synchronized EvaluationResponse evaluate(String input, boolean notifyListeners) {

        input = input.replace("\\", "\\\\");
        input = input.replace("\"", "\\\"");

        String inputText = "LoadScript(\"" + input + "\");";

        EvaluationResponse evaluationResponse = this.asynchronousInterpreter.evaluate(inputText, notifyListeners);

        return evaluationResponse;
    }


    private void initialize() {

        // Unassign all variables in the MathPiper synchronousInterpreter.
        EvaluationResponse response = evaluate("Unassign(*);");
        String result;

        if (response.isExceptionThrown()) {
            result = response.getException().getMessage();

            Log.e(TAG, result);

            Toast.makeText(this, result, Toast.LENGTH_SHORT).show();
            return;
        } else {
            result = response.getResult();
        }

        InputStream inputStream;
        try {

            String mpwFile = this.programFilename;

            inputStream = getAssets().open("programs/" + mpwFile);

            foldsMap = org.mathpiper.test.MPWSFile.getFoldsMap(mpwFile,
                    inputStream);

        } catch (Throwable e) {
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
            Log.e(TAG, e.getMessage());
        }

        // =================== Configure.

        Fold fold = foldsMap.get("configuration");

        if (fold == null) {
            String message = "Configuration information is missing.";

            Log.e(TAG, message);

            Toast.makeText(MathPiperCalc.this, message,
                    Toast.LENGTH_SHORT).show();

            return;
        }

        String codeText = fold.getContents();

        response = evaluate(codeText);

        if (response.isExceptionThrown()) {
            result = response.getException().getMessage();

            Log.e(TAG, result);

            Toast.makeText(this, result, Toast.LENGTH_SHORT).show();
            return;
        } else {
            result = response.getResult();
        }

        // =================== Information.
        fold = foldsMap.get("information");

        if (fold == null) {
            String message = "Exercise information is missing.";

            Log.e(TAG, message);

            Toast.makeText(MathPiperCalc.this, message,
                    Toast.LENGTH_SHORT).show();

            return;
        }

        codeText = fold.getContents();

        response = evaluate(codeText);

        if (response.isExceptionThrown()) {
            result = response.getException().getMessage();

            Log.e(TAG, result);

            Toast.makeText(this, result, Toast.LENGTH_SHORT).show();
            return;
        } else {
            result = response.getResult();
        }

        // ================== program.
        fold = foldsMap.get("program");

        if (fold == null) {
            String message = "The program is missing.";

            Log.e(TAG, message);

            Toast.makeText(MathPiperCalc.this, message,
                    Toast.LENGTH_SHORT).show();

            return;
        }

        this.programCode = fold.getContents();


        // log("[\"Tag\",\"VersionInfo\"],[\"AppVersion\",\"" + appVersion +"\"],[\"MathpiperVersion\",\"" + org.mathpiper.Version.version() + "\"]");
    }
}
