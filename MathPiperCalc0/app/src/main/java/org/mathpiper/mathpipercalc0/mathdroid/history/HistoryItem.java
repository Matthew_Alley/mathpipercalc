package org.mathpiper.mathpipercalc0.mathdroid.history;

public interface HistoryItem {

    String getFirst();

    String getSecond();

    String prettyPrint();

}
