package org.mathpiper.mathpipercalc0.mathdroid;

import android.content.*;
import android.util.*;
import android.view.inputmethod.*;
import android.widget.*;
import org.mathpiper.mathpipercalc0.R;

public class MathPiperCalcEditText extends EditText {
  public MathPiperCalcEditText(Context context, AttributeSet attrs) {
    super(context, attrs);
    Compatibility.get().fixEditText(this);
    setSelectAllOnFocus(true);

    setImeActionLabel(context.getString(R.string.exe_button), EditorInfo.IME_ACTION_DONE);
    setImeOptions(EditorInfo.IME_ACTION_DONE);
  }
}
