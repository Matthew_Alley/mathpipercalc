package org.mathpiper.mathpipercalc0.mathdroid.history;

import android.content.*;
import android.view.*;
import android.widget.*;
import java.util.*;


/**
 * A simple ListAdapter for the ListView that shows the history transcript.
 */
public class HistoryAdapter extends BaseAdapter {
    private final Context mContext;
    private final ArrayList<HistoryItem> mItems;

    public HistoryAdapter(Context context) {
        mContext = context;
        mItems = new ArrayList<HistoryItem>();
    }

    public void add(HistoryItem item) {
        mItems.add(item);
        notifyDataSetChanged();
    }


    public void remove(int index) {
        mItems.remove(index);
        notifyDataSetChanged();
    }

    public void clear() {
        mItems.clear();
        notifyDataSetChanged();
    }

    public int getCount() {
        return mItems.size();
    }

    public HistoryItem getItem(int index) {
        return mItems.get(index);
    }

    public long getItemId(int index) {
        return index;
    }

    public View getView(int index, View convertView, ViewGroup parent) {

        View returnView = null;

        HistoryItem historyItem = mItems.get(index);

        if(historyItem instanceof HistoryCalculationItem) {

            if (convertView == null || convertView instanceof HistoryProgramItemView) {
                returnView =  new HistoryCalculationItemView(mContext, (HistoryCalculationItem) historyItem);
            }
            else {
                HistoryCalculationItemView historyView = (HistoryCalculationItemView) convertView;
                historyView.setItem((HistoryCalculationItem) historyItem);
                returnView = historyView;
            }
        }
        else
        {
            // todo:tk:add support for convertView not being null.
            HistoryProgramItemView historyProgramItemView = new HistoryProgramItemView(mContext, "TODO");
            historyProgramItemView.addView(mContext,  ((HistoryProgramItems) historyItem).getAll());
            returnView = historyProgramItemView;
        }

        return returnView;
    }

    public void fromString(String serializedHistory) {
        if (serializedHistory == null || serializedHistory.length() == 0) {
            return;
        }
        String[] historyLines = serializedHistory.split("\n");
        for (int i = 0; i < historyLines.length;) {

            if(historyLines[i].contains(":")) {
                HistoryProgramItems historyProgramItems = new HistoryProgramItems();

                while (i < historyLines.length && historyLines[i].contains(":")) {
                    historyProgramItems.add(historyLines[i]);
                    i++;
                }
                add(historyProgramItems);
            }
            else {
                add(new HistoryCalculationItem(historyLines[i], historyLines[i + 1].substring(" = ".length())));
                i += 2;
            }
        }
    }


    // Used both as the serialized form when we save the current state.
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (HistoryItem item : mItems) {
            sb.append(item.toString());
            sb.append("\n");
        }



        for (int index = 0; index < mItems.size(); index++) {

            sb.append(mItems.get(index).toString());

            if(index < mItems.size()-1) {
                sb.append("\n");
            }
        }

        return sb.toString();
    }


    // Used as what "copy all" copies to the clipboard.
    public String prettyPrint() {

        StringBuilder sb = new StringBuilder();

        for (int index = 0; index < mItems.size(); index++) {

            if(mItems.get(index) instanceof HistoryCalculationItem) {
                sb.append(mItems.get(index));
            }
            else if(mItems.get(index) instanceof HistoryProgramItems){
                sb.append(mItems.get(index).prettyPrint());
            }
            else {
                // Todo:tk:internal error.
            }

            if(index < mItems.size()-1) {
                sb.append("\n");
            }
        }

        return sb.toString();
    }



}