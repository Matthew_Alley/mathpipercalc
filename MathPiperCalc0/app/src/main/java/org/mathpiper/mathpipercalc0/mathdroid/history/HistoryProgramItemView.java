package org.mathpiper.mathpipercalc0.mathdroid.history;

import android.content.*;
import android.widget.*;

import org.mathpiper.mathpipercalc0.R;

import java.util.ArrayList;
import java.util.List;

/**
 * A view for the items in the history transcript.
 * Basically just two text views: one for the question, another for the answer.
 */
public class HistoryProgramItemView extends LinearLayout {
    private String programName = "";
    private final List<TextView> textViews = new ArrayList<TextView>();

    public HistoryProgramItemView(Context context, String programName) {
        super(context);

        this.programName = programName;

        this.setOrientation(VERTICAL);
    }


    public void addView(Context context, String[] items)
    {
        for(String item: items) {

            String [] programItem = item.split(":");

            int appearance = R.style.history_question_appearance;
            String text = "ERROR";

            if(programItem[1].equals("question"))
            {
                appearance = R.style.history_question_appearance;
                text = programItem[0];
            }
            else if(programItem[1].equals("answer"))
            {
                appearance = R.style.history_answer_appearance;
                text = " = " + programItem[0];
            }
            else if(programItem[1].equals("inputQuestion"))
            {
                appearance = R.style.history_input_question_appearance;
                text = programItem[0];
            }
            else if(programItem[1].equals("inputValue"))
            {
                appearance = R.style.history_input_value_appearance;
                text = programItem[0];
            }
            else if(programItem[1].equals("programName"))
            {
                appearance = R.style.history_program_name_appearance;
                text = programItem[0];
            }
            else
            {
                // todo:tk:internal error.
            }

            TextView textView = new TextView(context);
            textView.setTextAppearance(context, appearance);
            textView.setText(text);
            addView(textView, new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT));
        }
    }



    @Override
    public String toString()
    {
        StringBuilder sb = new StringBuilder();

        sb.append(this.programName);

        for (TextView textView : textViews) {

            sb.append(textView.getText());

            sb.append("\n");
        }

        return sb.toString();
    }



    // Allows reuse of a recycled HistoryItemView.
    public void setItem(HistoryCalculationItem item) {

        // mQuestionView.setText(item.question);
    }

}