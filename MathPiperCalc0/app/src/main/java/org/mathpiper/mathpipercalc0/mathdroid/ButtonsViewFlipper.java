package org.mathpiper.mathpipercalc0.mathdroid;


import android.content.Context;
import android.util.AttributeSet;
import android.widget.ViewFlipper;

public class ButtonsViewFlipper extends ViewFlipper {
    public ButtonsViewFlipper(Context context) {
        super(context);

    }

    public ButtonsViewFlipper(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);

        //this.addView(new ButtonsView(context));

    }

    public void addButtons(ButtonsView buttonsView)
    {
        this.addView(buttonsView);
    }
}


