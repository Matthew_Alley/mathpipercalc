/*
 * Copyright (C) 2013 Ted Kosan
 * 
 * This file is part of MathPiper Exercise.
 * 
 * Foobar is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 * 
 * Foobar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Foobar. If not, see <http://www.gnu.org/licenses/>.
*/

package org.mathpiper.ui.gui.android;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import org.mathpiper.mathpipercalc0.R;


/**
 * A fragment representing a single Item detail screen. This fragment is either
 * contained in a {@link ExerciseItemListActivity} in two-pane mode (on tablets) or a
 * {@link ExerciseItemDetailActivity} on handsets.
 */
public class ExerciseItemDetailFragment extends Fragment {
	/**
	 * The fragment argument representing the item ID that this fragment
	 * represents.
	 */
	public static final String ARG_ITEM_ID = "item_id";

	/**
	 * The dummy content this fragment is presenting.
	 */
	private ExerciseManager.ExerciseInformation mItem;

	/**
	 * Mandatory empty constructor for the fragment manager to instantiate the
	 * fragment (e.g. upon screen orientation changes).
	 */
	public ExerciseItemDetailFragment() {
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		if (getArguments().containsKey(ARG_ITEM_ID)) {
			// Load the dummy content specified by the fragment
			// arguments. In a real-world scenario, use a Loader
			// to load content from a content provider.
			mItem = ExerciseManager.ITEM_MAP.get(getArguments().getString(
					ARG_ITEM_ID));
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		final View rootView = inflater.inflate(R.layout.fragment_item_detail,
				container, false);

		// Show the dummy content as text in a TextView.
		if (mItem != null) {
			String description = mItem.getDescription();
			((TextView) rootView.findViewById(R.id.item_detail))
					.setText(description);
			
			
			final Button button_launch_exercise = (Button) rootView.findViewById(R.id.button_launch_exercise);
			button_launch_exercise.setOnClickListener(new View.OnClickListener() {
				public void onClick(View view) {
					
					//SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(rootView.getContext());
					
					mItem.getFilename();
					
					Intent exerciseActivity = new Intent(rootView.getContext(),
							MathPiperExerciseActivity.class);
					
					exerciseActivity.putExtra("org.mathpiper.ui.gui.mpwsFile", "exercises/" + mItem.getFilename());
					startActivity(exerciseActivity);
				}
			});
		}

		return rootView;
	}
}
