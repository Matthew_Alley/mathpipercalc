/*
 * Copyright (C) 2013 Ted Kosan
 * 
 * This file is part of MathPiper Exercise.
 * 
 * Foobar is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as 
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 * 
 * Foobar is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with Foobar. If not, see <http://www.gnu.org/licenses/>.
*/

package org.mathpiper.ui.gui.android;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.mathpiper.interpreters.EvaluationResponse;
import org.mathpiper.interpreters.Interpreter;
import org.mathpiper.interpreters.Interpreters;
import org.mathpiper.lisp.variables.GlobalVariable;
import org.mathpiper.test.Fold;

import android.app.Activity;
import android.util.Log;
import android.widget.Toast;


public class ProgramManager {
	
	private static final String TAG = "ProgramManager";

    private Interpreter interpreter;

    private String programsPath = "programs";
	
	public ProgramManager(Activity activity) throws Throwable
	{
        ProgramManager.ITEMS.clear();
        ProgramManager.ITEM_MAP.clear();

        interpreter = Interpreters.getSynchronousInterpreter();

		String[] assets = activity.getAssets().list(programsPath);
		
		int itemNumber = 0;
		
		for(String asset: assets)
		{
			if(asset.endsWith(".mpws") || asset.endsWith(".MPWS"))
			{
				InputStream inputStream = activity.getAssets().open(programsPath + "/" + asset);
				
				Map<String, Fold> foldsMap = org.mathpiper.test.MPWSFile.getFoldsMap(asset, inputStream);
				
				Fold fold = foldsMap.get("information");

				if (fold == null) {
					String message = "Program information is missing.";

					Log.e(TAG, message);

					Toast.makeText(activity, message,
							Toast.LENGTH_SHORT).show();

					return;
				}

				String codeText = fold.getContents();

                codeText = codeText.replace("\\", "\\\\");
                codeText = codeText.replace("\"", "\\\"");

                String inputText = "LoadScript(\"" + codeText + "\");";

				EvaluationResponse response = interpreter.evaluate(inputText);

				String result;
				if (response.isExceptionThrown()) {
					result = response.getException().getMessage();

					Log.e(TAG, result);

					Toast.makeText(activity, result, Toast.LENGTH_SHORT).show();
					return;
				} else {
					result = response.getResult();
				}
				
				String programName = getGlobalString("programName");
				if (programName == null)
					programName = "<UNNAMED>";

				
				String programVersion = getGlobalString("programVersion");
				if (programVersion == null)
					programVersion = "<UNVERSIONED>";
				
				String programDescription = getGlobalString("programDescription");
				if (programDescription == null)
					programDescription = "<UNVERSIONED>";
				
				addItem(new ProgramInformation(String.valueOf(++itemNumber), programName, programDescription, asset));

			}
		}
		//activity.getAssets().open(mpwFile);
	}


    private String getGlobalString(String variableName) {
        GlobalVariable variable = interpreter.getEnvironment().iGlobalState
                .get(variableName);

        if (variable == null) {
            return null;
        }

        String variableString = variable.toString();
        variableString = org.mathpiper.lisp.Utility
                .stripEndQuotesIfPresent(variableString);

        return variableString.toString();
    }


	private static List<ProgramInformation> ITEMS = new ArrayList<ProgramInformation>();


	public static Map<String, ProgramInformation> ITEM_MAP = new HashMap<String, ProgramInformation>();



	private void addItem(ProgramInformation item) {
		ITEMS.add(item);
		ITEM_MAP.put(item.id, item);
	}


    public static List<ProgramInformation> getITEMS() {
        return ITEMS;
    }

    public static Map<String, ProgramInformation> getITEM_MAP() {
        return ITEM_MAP;
    }

    public static class ProgramInformation {
		private String id;
		private String name;
		private String description;
		private String filename;

		public ProgramInformation(String id, String name, String description, String filename) {
			this.id = id;
			this.name = name;
			this.description = description;
			this.filename = filename;
		}

		
		
		public String getId() {
			return id;
		}



		public void setId(String id) {
			this.id = id;
		}



		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getDescription() {
			return description;
		}

		public void setDescription(String description) {
			this.description = description;
		}
		
		
		
		public String getFilename() {
			return filename;
		}



		public void setFilename(String filename) {
			this.filename = filename;
		}



		public String toString()
		{
			return name;
		}


	}
}
